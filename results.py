# -*- coding: utf-8 -*-
"""
Created on Tue Oct 26 11:33:54 2021

@author: embedded10
"""

#%%
import numpy as np
import matplotlib.pyplot as plt
import os
import imp

from IPython import get_ipython

os.add_dll_directory("C://Program Files//Lumerical//v212//api//python")
lumapi = imp.load_source("lumapi","C://Program Files//Lumerical//v212//api//python//lumapi.py")

INTER = lumapi.INTERCONNECT("qpsk_back2back_hybrid_v2.icp")
get_ipython().run_line_magic('matplotlib', 'qt')

#%%
# Signal I

sigI = INTER.getresult("OSC_1","signal")
sigI_time = sigI['time']
sigI_amp  = sigI['amplitude (a.u.)']

plt.figure(figsize=(10,6))
plt.plot(sigI_time,sigI_amp, linewidth=2.0, label = 'Signal-I')
plt.xlabel('Time (s)',fontsize=20.0);
plt.ylabel('Amplitude (a.u.)',fontsize=20.0);
plt.legend(fontsize=18.0);
plt.grid()
plt.show()

# Signal Q

sigQ = INTER.getresult("OSC_2","signal")
sigQ_time = sigQ['time']
sigQ_amp  = sigQ['amplitude (a.u.)']

plt.figure(figsize=(10,6))
plt.plot(sigQ_time,sigQ_amp, linewidth=2.0, label='Signal-Q')
plt.xlabel('Time (s)',fontsize=20.0);
plt.ylabel('Amplitude (a.u.)',fontsize=20.0);
plt.legend(fontsize=18.0);
plt.grid()
plt.show()

# Constelação

constI = INTER.getresult("VSA_1","waveform/constellation I")
constI = constI['amplitude (a.u.)']
constQ = INTER.getresult("VSA_1","waveform/constellation Q")
constQ = constQ['amplitude (a.u.)']

plt.figure(figsize=(10,6))
plt.plot(constI, constQ,'o', linewidth=3.0)
plt.xlabel('I (a.u.)',fontsize=20.0);
plt.ylabel('Q (a.u.)',fontsize=20.0);
plt.grid()
plt.show()
